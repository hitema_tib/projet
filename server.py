from flask import Flask, render_template, request, jsonify, send_from_directory
import os
from werkzeug.utils import secure_filename
import json
import datetime

app = Flask(__name__)

UPLOAD_FOLDER = 'images'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def index():
    return render_template('index.html')

# Route pour servir les images
@app.route('/images/<path:filename>')
def serve_image(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/upload', methods=['POST'])
def upload_file():
    id = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    if 'imageUpload' not in request.files:
        return 'No file part'
    
    file = request.files['imageUpload']

    if file.filename == '':
        return 'No selected file'
    
    if file:
        filename = id
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return id

@app.route('/add_user', methods=['POST'])
def add_user():
    new_user = request.get_json()

    with open('users.json', 'r') as file:
        users = json.load(file)
        users.append(new_user)

    with open('users.json', 'w') as file:
        json.dump(users, file, indent=4)
    
    return jsonify({"message": "User added successfully"})

@app.route('/delete_user/<username>', methods=['DELETE'])
def delete_user(username):
    with open('users.json', 'r') as file:
        users = json.load(file)
        users = [user for user in users if user['username'] != username]

    with open('users.json', 'w') as file:
        json.dump(users, file, indent=4)
    
    return jsonify({"message": f"User {username} deleted successfully"})

@app.route('/load_users', methods=['GET'])
def load_users():
    with open('users.json', 'r') as file:
        users = json.load(file)
    
    return jsonify(users)


@app.route('/edit_user/<username>', methods=['PUT'])
def edit_user(username):
    updated_user = request.get_json()

    with open('users.json', 'r') as file:
        users = json.load(file)
        for user in users:
            if user['username'] == username:
                user.update(updated_user)
                break

    with open('users.json', 'w') as file:
        json.dump(users, file, indent=4)
    
    return jsonify({"message": f"User {username} updated successfully"})

if __name__ == '__main__':
    if not os.path.exists('users.json'):
        with open('users.json', 'w') as file:
            json.dump([], file)
    app.run(debug=True, host='0.0.0.0', port=5000)
